### Dependencies:
raspberry:
```
  sudo apt update && sudo apt upgrade
  sudo apt install dnsmasq tshark driftnet dsniff squid3 apache2 screen


# optional :  
  sudo apt install git # for downloading this repo 😄
  sudo apt install wireshark
  sudo apt install vim
```

Client:

`sudo apt install rsync sshpass nmap awk dnsniff chaosreader`

---
#### After initial bootup :
```
  git clone https://bitbucket.org/Ja0Dar/sniffpi.git
  #[maybe chmod  u+x for sh files]
  cd sniffpi
  sudo ./install.sh <wlan0|eth1>

  # TODO - check if it configures wireshark properly ( pcaps should be created as non-root)
  # and maybe https://superuser.com/a/444387 for non-sudo dumpcap
```

---
### Resources
    1. https://pimylifeup.com/raspberry-pi-wifi-bridge/  
    2. https://www.makeuseof.com/tag/how-to-make-a-wifi-network-that-only-transmits-cat-pictures-with-a-raspberry-pi/


------
Handy http sites :
* joemonster.org
* kompilatory.agh.edu.pl
* lubos.rendek.org/remove-all-iptables-prerouting-nat-rules/



# TODO WZ
1. Cats.pl in python
2. changing images python script [ *please avoid redundant code 😏*]
  * rotating
  * blurring
  * watermarking (?)
  * pizza ?
3. Phishing website [ down more details ]
4. Prompt for interfacing with everything.

Prompt functioalities :
* turn on/off squid use iptables' scripts. → `config_editing/set_iptables.sh`
* change squid photo scripts
* dns spoofing
  * **create example of malicious website that could log & spoof data**
    * it should be pushed to `/var/www/index.html`
    * idea : show dns spoofing power. unfortunetely it is hard to do it with htpps ;)
  * edit temporary dns  hoss file (`config_editing/tmp_cofigs/dnsmasq.hosts`)// you can do this in memory also and just push overriding
    * add /remove entries
    * push tmp dns file ( `config_editing/push_dns_hosts.sh`)*

* list & change squid conf scripts  ( using `config_editing/push_squid.sh`)
* run wireshark  `dumping_scripts/wireshark.sh`
* sync files `clinent/rsync.sh`
* create log file `client/read_urls_form_pcaps.sh`
* create html from pcap `client/generate_html_from_pcap.sh`
* show status ( wireshark, squid, actual dns hosts ( form /etc/dnsmasq.hosts))



---
# TODO JD
1. Syncing through rsync #Done
2. dumping pcap to  dir #Done
3. url from pcap #Done
4. change script to squid #Done
4. pcap overview  #Done ( `client/generate_html_from_pcap.sh`)
5. Dns spoofing. # Done
6. Init scripts
  * init→ Done
  * todo : remove necesity for dnsmasq restart after bootup
