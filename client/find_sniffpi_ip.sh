#!/bin/bash

if [[  $# -eq 0 || ($# -eq 1 && ( "$1" == "-h" || "$1" == "--help")) ]];then
echo $0 [network]
echo "network example : 192.168.0.0/24"
exit 0
fi

nmap $1 -p22,80,873,3128 --open -oG - | awk '/22\/open.*80\/open/{print $2}'
