#!/bin/bash

# it shoud be run on client machine
#for some reason rsync.sh -help works as weird recursive ls
if [[ $# -eq 0 || ($# -eq 1 && ( "$1" == "-h" || "$1" == "--help")) ]];then
echo $0 [remote_ip] [user] [password] [remote_path] [local_path]
exit 0
fi

ip=$1
user=${2:-pi}
password=${3:-raspberry}
remote_path=${4:-"/home/pi/sniffpi/dumps"} # TODO - do not hardcode?
local_path=${5:-"."}

sshpass -p $password rsync -re ssh $user@$ip:$remote_path $local_path
