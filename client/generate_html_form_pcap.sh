#!/bin/bash
if [[ $# -eq 0 || ($# -eq 1 && ( "$1" == "-h" || "$1" == "--help")) ]];then
echo $0 directory [--browser]
exit 0
fi

rm $1/dump.pcap
mkdir $1/html
chaosreader -D $1/html $1/*.pcap

if [[ $# -eq 2 && $2 == "--browser" ]]
then
  sensible-browser $1/html/index.html
fi
