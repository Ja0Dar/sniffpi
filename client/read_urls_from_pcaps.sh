#!/bin/bash
if [[ $# -eq 0 || ($# -eq 1 && ("$1" == "-h" || "$1" == "--help")) ]]
  then
    echo "Usage: $0 dir [output_file]"
    exit
  fi

for file in $1/*.pcap ; do
  if [ $# -eq 1 ]
  then
   urlsnarf -p $file | cut -d" " -f7
 else
  urlsnarf -p $file| cut -d" " -f7  >> $2
  fi
done
