import getpass
import os
from cmd import Cmd

from paramiko import AuthenticationException

from sshconnector import SshServant
from tabulate import tabulate
from helpers import *

sniffpi_router_addr = "192.168.220.1"


class SnifferPrompt(Cmd):
    def __init__(self, ssh: SshServant):
        super().__init__()
        self.ssh = ssh
        self.login()
        self.home_name, self.home, self.iface_name, self.iface = get_env_variables(self.ssh)

    def login(self):
        network = input("Enter network addr/mask: ")
        addr = find_raspi_ip(network).replace("\n", "")
        password = getpass.getpass("Password: ")
        user = "pi"
        self.ssh.config = (addr, user, password)
        try:
            self.ssh.login()
            print("Login successful")
            return
        except AuthenticationException:
            print("Unable to log in, please try again")
            self.login()

    def do_proxy(self, args):
        """
        Proper usage of command is:
        proxy <scripts [activate <script>] | status | start | stop | help>
        """
        args = args.split(' ')
        if args[0] == "scripts" and len(args) == 1:
            result = create_scripts_table(self.ssh)
            print(tabulate(result, headers=("Script", "Active")))
        elif len(args) == 3 and args[1] == "activate":
            if is_script_in_catalogue(self.ssh, args[2]):
                print("Beginning script activation ...")
                activate_script(self.ssh, args[2])
            else:
                print("Unable to find a proper script. "
                      "Check proxy scripts to choose one")

        elif args[0] == "status":
            c = "sudo -E {}={} {}={} {}/iptables/view_routes.sh".format(
                self.home_name, self.home, self.iface_name, self.iface, self.home)
            out, err = execute_command(self.ssh, c, "Something gone wrong", '')
            if 'redir' in out:
                print("Proxy is activated")
            else:
                print("Proxy is inactive")

        elif args[0] == "start":
            c = "sudo -E {}={} {}={} {}/config_editing/set_iptables.sh".format(
                self.home_name, self.home, self.iface_name, self.iface, self.home)
            execute_command(self.ssh, c,
                            "Errors occurred during starting proxy",
                            "Proxy started")

        elif args[0] == "stop":
            c = "sudo -E {}={} {}={} {}/config_editing/set_iptables.sh --no-squid".format(
                self.home_name, self.home, self.iface_name, self.iface, self.home)
            execute_command(self.ssh, c,
                            "Errors occured during stopping proxy",
                            "Proxy stopped")
        else:
            print(self.do_proxy.__doc__)

    def do_wireshark(self, args):
        """
        Proper usage of command is:
        wireshark <status | start | stop >
        """
        args = args.split(' \t')
        if args[0] == "status":
            if is_running(self.ssh, "dumpcap", 2):
                print("Active")
            else:
                print("Inactive")
        elif args[0] == "start":
            # STILL NOT WORKING >:(
            c = "{}={} screen -d -m {}/dumping_scripts/wireshark.sh".format(
                self.home_name, self.home, self.home)
            execute_command(self.ssh, c, "Unable to start capturing",
                            "Capturing started")

        elif args[0] == "stop":
            execute_command(self.ssh, "sudo killall wireshark.sh",
                            "Something went Wrong", "Wireshark stopped.")
        else:
            print(self.do_wireshark.__doc__)

    def do_sync(self, args):
        """
        Sync method does not obtain parameters.
        """
        args = args.split(' \t')
        if args[0] == '':
            conf = self.ssh.config
            command = "./rsync.sh {} {} {} ".format(conf[0], conf[1], conf[2])
            os.system(command)
        else:
            print(self.do_sync.__doc__)

    def do_log(self, args):
        """
        log <html> | <file [out]>
        """
        if ' ' in args:
            args = args.split(' ')
        else:
            args = [args]
        if args[0] == 'html':
            c = "./generate_html_form_pcap.sh dumps"
            os.system(c)
            print("Done!")
        elif args[0] == 'file' and len(args) == 2:
            file = args[1]
            c = "./read_urls_from_pcaps.sh dumps {}".format(
                file)
            os.system(c)
        else:
            print(self.do_log.__doc__)

    def do_dns(self, args):
        """
        Proper usage of command is:
        dns <status | add <Inet4Addr> <DnsDomain> [DnsDomain].. >
        """
        args = args.split(' ')
        if args[0] == "status":
            out, err = self.ssh.send_command("cat /etc/dnsmasq.hosts")
            print(out)
        elif args[0] == "add" and len(args) >= 3:
            ip = args[1].replace("<self>", sniffpi_router_addr)
            hosts_str = " ".join(args[2:len(args)])
            hosts = args[2:len(args)]
            final = "{}\t{}".format(ip, hosts_str)
            if is_valid_ip(ip) and are_hosts_valid(hosts):
                c0 = "sudo {}={} echo {} >> {}/config_editing/tmp_configs/dnsmasq.hosts".format(
                    self.home_name, self.home, final, self.home)
                out, err = self.ssh.send_command(c0)
                print(out, err)
                c = "sudo {}={} {}/config_editing/push_dns_hosts.sh".format(
                    self.home_name, self.home, self.home)
                out, err = self.ssh.send_command(c)
            else:
                print("Arguments are not valid")
        elif args[0] == "clear":

            c0 = "{}={} > {}/config_editing/tmp_configs/dnsmasq.hosts".format(
                self.home_name, self.home, self.home)
            out, err = self.ssh.send_command(c0)
            print(out, err)
            c = "sudo {}={} {}/config_editing/push_dns_hosts.sh".format(
                self.home_name, self.home, self.home)
            out, err = self.ssh.send_command(c)

        else:
            print(self.do_dns.__doc__)

    def do_quit(self, args):
        self.ssh.logout()
        print("Bye bye.")
        raise SystemExit
