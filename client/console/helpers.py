import os
import socket

import re

# home_name, home = "SNIFFPI_LOC", ""
# iface_name, iface = "SNIFFPI_IFACE", ""


def is_valid_ip(ip):
    try:
        socket.inet_aton(ip)
        return True
    except socket.error:
        return False


def are_hosts_valid(hosts):
    if isinstance(hosts, str):
        hosts = [hosts]
    for x in hosts:
        if not _is_valid_hostname(x):
            return False
    return True


def find_raspi_ip(network):
    ip = os.popen("./find_sniffpi_ip.sh {}".format(network)).read()
    return ip


def _is_valid_hostname(hostname):
    if len(hostname) > 255:
        return False
    if hostname[-1] == ".":
        hostname = hostname[
                   :-1]  # strip exactly one dot from the right, if present
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))


def get_env_variables(ssh):
    global home,home_name,iface,iface_name
    if not "env" in get_env_variables.__dict__.keys():
        stdout, err = ssh.send_command("cat /home/pi/sniffpi.conf")
        lists = stdout.split('\n')
        home_name, home = lists[0].split(' ')
        iface_name, iface = lists[1].split(' ')
        print(home_name, home, iface_name, iface)
        get_env_variables.env=(home_name,home,iface_name,iface)
    return get_env_variables.env


def create_scripts_table(ssh):
    scripts, err = ssh.send_command("ls %s/photo_scripts/" % home)
    if err is not '':
        print(err)
        return
    scripts = [x for x in scripts.split("\n")]
    scripts.remove('')
    active = ssh.send_command(
        "cat /etc/squid/squid.conf | grep url_rewrite_program")[0].split(
        "/").pop().replace('\n', '')
    result = [(script, script == active) for script in scripts]
    return result


def is_script_in_catalogue(ssh, file,
                           dir=None) -> bool:

    home_name,home,iface_name,iface = get_env_variables(None)
    if dir is None:
        dir="{}/photo_scripts".format(home)
    scripts, err = ssh.send_command("ls %s" % dir)
    scripts = [x.strip('\n') for x in scripts.split('\n')]
    return file in scripts


def is_running(ssh, name, lines: int):
    l = ssh.send_command("ps -aux | grep %s | wc -l" % name)[0]
    return int(l) > lines


def activate_script(ssh, file, dir = None):
    home_name,home,iface_name,iface = get_env_variables(None)

    if dir is None:
        dir="{}/photo_scripts".format(home)

    prefix = ''
    if '.py' in file:
        prefix = "/usr/bin/python3"

    c = "sudo {}={} {}/config_editing/push_squid.sh \"{} {}\"".format(
        home_name, home, home, prefix,
        dir) + "/" + file

    stdout, stderr = ssh.send_command(c)
    if stderr != '':
        print(stderr)
        print("Activation Failed :(")
        return
    else:
        print("Script activated successfully")
    return


def execute_command(ssh, comm, msgfail, msgok):
    out, err = ssh.send_command(comm)
    if err == '':
        if not out == '':
            print(out)
        if not msgok == '':
            print(msgok)
    else:
        print(err)
        print(msgfail)
    return out, err


def start_service(ssh, name):
    out, err = ssh.send_command("sudo service %s restart" % name)
    if err == '':
        print("Service started successfully.")
    else:
        print(err, "Errors occurred while starting service")


def stop_service(ssh, name):
    out, err = ssh.send_command("sudo service %s stop" % name)
    if err == '':
        print("Service stopped successfully.")
    else:
        print(err, "Errors occurred while stopping service")
