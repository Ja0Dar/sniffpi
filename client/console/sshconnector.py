import paramiko as myssh


class SshServant:
    def __init__(self):
        self.ssh = myssh.SSHClient()
        self.config = ("network", "pi", "raspberry")
        self.STATUS_OK = True
        self.STATUS_BAD = False
        self.ssh.set_missing_host_key_policy(myssh.AutoAddPolicy())

    def login(self) -> bool:
        if not self.ssh.connect(self.config[0], 22, self.config[1],
                                self.config[2]):
            return self.STATUS_BAD
        return self.STATUS_OK

    def send_command(self, command) -> (str, str):
        stdin, stdout, stderr = self.ssh.exec_command(command, get_pty=False)
        return stdout.read().decode("utf-8").replace("\r",
                                                     ""), stderr.read().decode(
            "utf-8").replace("\r", "")

    def send_via_channel(self, command) -> (str, str):
        chan = self.ssh.invoke_shell()
        chan.send(command)
        return chan.recv(65536).decode("utf-8"), chan.recv_stderr(
            65536).decode("utf-8")

    def logout(self):
        self.ssh.close()
