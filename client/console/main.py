from sshconnector import SshServant
from console import SnifferPrompt

if __name__ == '__main__':
    servant = SshServant()
    prompt = SnifferPrompt(servant)
    prompt.prompt = 'hacker@sniffer> '
    prompt.cmdloop("""

   _____       _  __  __       _
  / ____|     (_)/ _|/ _|     (_)
 | (___  _ __  _| |_| |_ _ __  _
  \___ \| '_ \| |  _|  _| '_ \| |
  ____) | | | | | | | | | |_) | |
 |_____/|_| |_|_|_| |_| | .__/|_|
                        | |
                        |_|

by Jak0Dar & zdzarsky
""")