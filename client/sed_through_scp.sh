#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
echo $0 remote_ip local_path [user] [password] [remote_path]
exit 0
fi

ip=$1
local_path=$2
user=${3:-pi}
password=${4:-raspberry}
remote_path=${5:-"/var/www"}

sshpass -p "$password" scp -r $local_path $user@$ip:$remote_path
