#!/bin/bash

if [ $# -eq 2 ]
then
	
	cat $1 | sed "s|url_rewrite_program .*\$|url_rewrite_program $2|"
else
	echo "
	$0 filename replacescript
	filename - path to squid.conf file
	replacescript - absolute path to replacing script
	"
fi
