#!/bin/bash
# it changes it only for this bootup
if [[ $# -eq 0 || ($# -eq 1 && ($1 == "--help" || $1 == "-h"))  ]]
then
  echo "Script to use if install_bootup wasn't configured."
  echo "$0 <wlan0|eth1> [--no-squid]"
  exit 0
fi

interface=$1
no_squid=${2:-""}


if [[ $no_squid == "--no-squid" ]]
then
  filename="no_squid.sh"
else
  filename="squid.sh"
fi

cd $SNIFFPI_LOC
sudo iptables/clear.sh
export SNIFFPI_IFACE=interface
sudo -E iptables/$filename && sudo service dhcpcd restart && sleep 13 && sudo service dnsmasq restart
