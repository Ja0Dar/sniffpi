#!/bin/bash

if [[ $# -eq 1 && ($1 == "--help" || $1 == "-h") ]]
then
  echo "$0  [--no-squid]"
  echo "interface is from env variable SNIFFPI_IFACE"
  exit 0
fi

no_squid=${1:-""}


if [[ $no_squid == "--no-squid" ]]
then
  filename="no_squid.sh"
else
  filename="squid.sh"
fi

cd $SNIFFPI_LOC/iptables
sudo -E ./clear.sh
sudo -E ./$filename
sudo sh -c "iptables-save  > /etc/iptables.ipv4.nat"

printf "#!/bin/bash \n iptables-restore < /etc/iptables.ipv4.nat \n exit 0"| sudo tee /etc/rc.local >& /dev/null
