#!/bin/bash

squid_loc="/etc/squid/squid.conf"
cd $SNIFFPI_LOC/config_editing

if [ $# -eq 1 ]
then

  if [[ ( "$1" == "-h" || "$1" == "--help") ]]
  then
    echo "Usage: sudo $0 rewrite_scirpt_path"
    exit 1
  else
    auxiliary/change_squidconf.sh configs/squid.conf "$1" | sudo tee $squid_loc >& /dev/null
  fi
else
  cat configs/squid.conf | sudo tee $squid_loc >& /dev/null
fi

sudo service squid restart
