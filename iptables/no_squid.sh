#!/bin/bash
sudo iptables -t nat -A POSTROUTING -o $SNIFFPI_IFACE -j MASQUERADE
sudo iptables -A FORWARD -i $SNIFFPI_IFACE -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o $SNIFFPI_IFACE -j ACCEPT
