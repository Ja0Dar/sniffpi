#!/bin/bash
sudo iptables -t nat -A POSTROUTING -o $SNIFFPI_IFACE -j MASQUERADE
sudo iptables -A FORWARD -i $SNIFFPI_IFACE -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o $SNIFFPI_IFACE -j ACCEPT


iptables -t nat -A POSTROUTING -j MASQUERADE
iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 80 -j DNAT --to-destination 192.168.220.1:3128
iptables -t nat -A PREROUTING -i $SNIFFPI_IFACE -p tcp -m tcp --dport 80 -j REDIRECT --to-port 3128
