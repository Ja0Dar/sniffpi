#!/usr/bin/python3
import binascii
import os
import sys

img_extensions = ['.gif', '.jpg', '.jpeg', '.png', '.bmp']
modification = "gray"


def url_to_fn(url: str) -> str:
    ext = url.split(".")[-1]
    prefix = binascii.hexlify(str.encode(url)).decode()
    return "{}_{}.{}".format(prefix, modification, ext)


def fn_to_url(fn: str) -> str:
    prefix = fn.split("_")[0]
    return binascii.unhexlify(str.encode(prefix)).decode()


def modify_url(url):
    if any(ext in url for ext in img_extensions):

        fn = url_to_fn(url)
        file_path = ("/var/www/images/{}".format(fn))
        result_url = ("http://192.168.220.1/images/{}".format(fn))

        if not os.path.isfile(file_path):
            os.system("/usr/bin/wget -q -O {} {}".format(file_path, url))
            os.chmod(file_path, 0o777)
            os.system("convert -colorspace Gray {} {}".format(file_path, file_path))

        return result_url
    return url


while True:
    line = sys.stdin.readline().strip()
    modified_url = modify_url(line)
    print(modified_url)
    sys.stdout.flush()
    os.system("echo \" {} -> {} \" | >> /tmp/{}.log".format(line, modified_url,modification))
