#!/usr/bin/perl
$|=1;
$count = 0;
$pid = $$;
#$ip="127.0.0.1";
$ip="192.168.220.1";
open (DEBUG, '>>/tmp/cats.log');
autoflush DEBUG 1;
print DEBUG "########################################################################\n";

while (<>) {
        chomp $_;
        if (m/nosquid/) {
              print DEBUG "Input NOSQUID: $url\n";
              print "$_\n";
              print DEBUG "Output NOSQUID: $_\n";
        }
        elsif ($_ =~ /(.*\.jpg)/i) {
                $url = $1;
                print DEBUG "Input: $url\n";
                system("/usr/bin/wget", "-q", "-O","/var/www/images/$pid-$count.gif", "http://thecatapi.com/api/images/get?format=src&type=gif&nosquid");
                chmod 0777,"/var/www/images/$pid-$count.gif";
                print DEBUG "http://$ip/images/$pid-$count.gif\n";
                print "http://$ip/images/$pid-$count.gif\n";
        }
        elsif ($_ =~ /(.*\.gif)/i) {
                $url = $1;
                print DEBUG "Input: $url\n";
                system("/usr/bin/wget", "-q", "-O","/var/www/images/$pid-$count.gif", "http://thecatapi.com/api/images/get?format=src&type=gif&nosquid");
                chmod 0777,"/var/www/images/$pid-$count.gif";
                print DEBUG "http://$ip/images/$pid-$count.gif\n";
                print "http://$ip/images/$pid-$count.gif\n";
        }
        elsif ($_ =~ /(.*\.png)/i) {
                $url = $1;
                print DEBUG "Input: $url\n";
                system("/usr/bin/wget", "-q", "-O","/var/www/images/$pid-$count.gif", "http://thecatapi.com/api/images/get?format=src&type=gif&nosquid");
                chmod 0777,"/var/www/images/$pid-$count.gif";
                print DEBUG "http://$ip/images/$pid-$count.gif\n";
                print "http://$ip/images/$pid-$count.gif\n";
        }
        elsif ($_ =~ /(.*\.jpeg)/i) {
                $url = $1;
                print DEBUG "Input: $url\n";
                system("/usr/bin/wget", "-q", "-O","/var/www/images/$pid-$count.gif", "http://thecatapi.com/api/images/get?format=src&type=gif&nosquid");
                chmod 0777,"/var/www/images/$pid-$count.gif";
                print DEBUG "http://$ip/images/$pid-$count.gif\n";
                print "http://$ip/images/$pid-$count.gif\n"; 
        }
        else {
                print "$_\n";
        }
        $count++;
}
