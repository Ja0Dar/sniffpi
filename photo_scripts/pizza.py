#!/usr/bin/python3
import sys
from urllib.request import urlopen

import os
from PIL import ImageFile

img_extensions = ['.gif', '.jpg', '.jpeg', '.png', '.bmp']


def getsizes(uri, default=(256, 256)):
    # get file size *and* image size (None if not known)
    with urlopen(uri) as file:
        p = ImageFile.Parser()
        while 1:
            data = file.read(1024)
            if not data:
                break
            p.feed(data)
            if p.image:
                return p.image.size
        return default


class UrlModifier():
    def __init__(self, id):
        self.prefix_id = id
        self.count = 0

    def modify_url(self, url):
        if any(ext in url for ext in img_extensions):
            witdth, height = getsizes(url)
            filename = "images/{}-{}.jpg".format(self.prefix_id, self.count)
            os.system("/usr/bin/wget -q -O /var/www/{} https://lorempizza.com/{}/{}".format(filename, witdth, height))
            os.chmod("/var/www/{}".format(filename), 0o777)
            return "http://192.168.220.1/{}".format(filename)
        return url


url_modifier = UrlModifier(os.getpid())
while True:
    line = sys.stdin.readline().strip()
    modified_url = url_modifier.modify_url(line)
    print(modified_url)
    sys.stdout.flush()
    os.system("echo \" {} -> {} \" | >> /tmp/pizza.log".format(line, modified_url))
