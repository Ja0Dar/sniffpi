#!/bin/bash


if [[ $# -eq 1 && ( "$1" == "-h" || "$1" == "--help") ]];then
echo "$0 [file change interval] [nr of fliles (cyclic buffer )] [sniffing duration] [filename]"
exit 0
fi




duration=${1:-120}
files=${2:-5}
endDuration=${3:-1200}
file=${4:-"$SNIFFPI_LOC/dumps/dump.pcap"}
dirname=`echo $file | sed "s/[a-zA-Z0-9._]*$//"`
mkdir -p $dirname
chmod 755 $dirname
touch $file
chmod 755 $file

# interface maybe should be dynamically set
dumpcap -P -i1 -b duration:$duration -b files:$files -a duration:$endDuration -w $file
