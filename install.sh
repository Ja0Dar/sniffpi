#!/bin/bash
if [[ $# -eq 0 || ($# -eq 1 && ($1 == "--help" || $1 == "-h"))  ]]
then
  echo "$0 sniffpi_interface"
  echo "sniffpi_interface : <wlan0|eth1>"
  exit 0
fi

# set interface variable
SNIFFPI_IFACE=$1
export SNIFFPI_IFACE=$SNIFFPI_IFACE
iface_export="export SNIFFPI_IFACE=$SNIFFPI_IFACE"

SNIFFPI_LOC=/home/pi/sniffpi
export SNIFFPI_LOC=$SNIFFPI_LOC
loc_export="export SNIFFPI_LOC=$SNIFFPI_LOC"

bashrc_loc=/home/pi/.bashrc
config_loc=/home/pi/sniffpi.conf

grep_res=`cat $bashrc_loc | grep SNIFFPI_LOC`
if  [ "" == "$grep_res" ]
then
  echo appending to bashrc
  printf "$iface_export\n $loc_export" >> $bashrc_loc
else
  echo overriding to bashrc
  cat $bashrc_loc | sed "s|^export SNIFFPI_IFACE.*$|$iface_export|" | sed "s|^export SNIFFPI_LOC.*$|$loc_export|" > tmp
  mv tmp $bashrc_loc
  sudo -E chown pi:pi $bashrc_loc
fi


printf "SNIFFPI_LOC $SNIFFPI_LOC\nSNIFFPI_IFACE $SNIFFPI_IFACE" > $config_loc
sudo -E chown pi:pi $config_loc
echo "Created $config_loc"

sudoers_line="pi ALL = NOPASSWD:SETENV: ALL"
grep_sudoers=`sudo cat /etc/sudoers |grep NOPASSWD`
if [ "" == "$grep_sudoers" ]
then
  echo $sudoers_line |sudo tee -a /etc/sudoers >& /dev/null
fi

cd $SNIFFPI_LOC/config_editing
#enable ip forwarding
sudo -E auxiliary/ip_forwarding.sh

# dnsmasq
cat configs/dns/dnsmasq.conf | sudo tee /etc/dnsmasq.conf >& /dev/null
cat configs/dns/dnsmasq.hosts | sudo tee /etc/dnsmasq.hosts >& /dev/null

#dhcpcd
cat configs/dhcpcd.conf | sudo tee /etc/dhcpcd.conf >& /dev/null

#resolvconf
cat configs/resolvconf.conf | sudo tee /etc/resolvconf.conf >& /dev/null

#squid && apache
cat configs/squid.conf | sudo tee /etc/squid/squid.conf >& /dev/null
squid -z

sudo mkdir /var/www/images
sudo chmod g+x /var/www
sudo chmod 777 /var/www/images
sudo usermod -a -G www-data proxy
sudo usermod -a -G www-data pi
sudo chown www-data:www-data /var/www
sudo chown www-data:www-data /var/www/images
touch /tmp/cats.log
chmod 777 /tmp/cats.log

cat configs/000-default.conf |sudo tee /etc/apache2/sites-available/000-default.conf >& /dev/null
sudo cp $SNIFFPI_LOC/config_editing/configs/index.html /var/www
sudo chmod 755 /var/www/index.html

sudo groupadd wireshark
sudo usermod -a -G wireshark pi
sudo chgrp wrieshark /usr/bin/dumpcap
sudo chmod 754 /usr/bin/dumpcap
sudo setcap 'CAP_NET_RAW+eip CAP_NET_ADMIN+eip' /usr/bin/dumpcap

sudo -E $SNIFFPI_LOC/config_editing/set_iptables.sh
